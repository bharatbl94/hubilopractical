//
//  Utils.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 24/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import Foundation
import SDWebImage
import SystemConfiguration

let TAG_PROGRESS_VIEW = 0x999
let DATA_LIST_LIMIT = 20

let networkUnavailable = "Network Not Available"


public class Utils{
   
    class func isInternetConnected() -> Bool{
        if !Reachability.isConnectedToNetwork(){
            return false
        } else {
            return true
        }
    }
    
    class func getImageLink(imageId: String) -> String {
           return "https://www.artic.edu/iiif/2/\(imageId)/full/843,/0/default.jpg"
       }
    
    class func showAlertDialog(_ viewController : UIViewController,_ title : String?,_ message : String,_ callback: ((Bool) -> Void)? = nil, btnText : String = "OK"){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let submitAction = UIAlertAction(title: btnText, style: .default , handler:{(UIAlertAction) in
            callback?(true)
        })
        alert.addAction(submitAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}

public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}

extension UIImageView{
    func loadImage(from url: String, _ completionHandler: ((SDExternalCompletionBlock) -> Void)? = nil  ){
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.sd_setImage(with: URL(string: url), placeholderImage: nil, options: .allowInvalidSSLCertificates, completed: nil)
    }
}
