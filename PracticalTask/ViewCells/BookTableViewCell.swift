//
//  BookTableViewCell.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 24/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var bookDescript: UILabel!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var imgCheckIsBookmarked: UIImageView!
    @IBOutlet weak var imageBook: UIImageView!
    var completionHandler:(() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func actionSelectBook(_ sender: Any) {
        if let callBackHandle = completionHandler{
            callBackHandle()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
