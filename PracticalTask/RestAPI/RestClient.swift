
import Foundation
import Alamofire

class RestClient{

    class func getBookList(pageIndex: Int = 1) -> DataRequest {
        return RestRequest.request(get: "artworks?page=\(pageIndex)&limit=\(DATA_LIST_LIMIT)")
    }
    
}

