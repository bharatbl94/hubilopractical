
import Foundation
import Alamofire
import AlamofireObjectMapper

class NetworkManager{
    static func request(_ fullUrl : String, method: HTTPMethod,  headers: HTTPHeaders,parameters: Parameters? = nil) -> DataRequest{
    
        return Alamofire.request(fullUrl, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON{response in
                switch response.result{
                case .success(let data):
                    let json = data as? NSDictionary
                    break
                case .failure(let error):
                    break
                }
        
        }
    }
}
