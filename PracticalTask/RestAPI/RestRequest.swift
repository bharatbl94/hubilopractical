import Alamofire

class RestRequest: NSObject {
    
    static let CONTENT_TYPE_VALUE = "application/json"
    static let CONTENT_TYPE_KEY = "Content-Type"
    static let mainURL = "https://api.artic.edu/api/v1/"

    static func request(get url: String) -> DataRequest{
        let fullUrl = mainURL + url
        let headers: HTTPHeaders = [CONTENT_TYPE_KEY: CONTENT_TYPE_VALUE]
        return NetworkManager.request(fullUrl, method: .get, headers: headers)
    }
    
    static func request(post url: String) -> DataRequest{
        let fullUrl = mainURL + url
        let headers: HTTPHeaders = [CONTENT_TYPE_KEY: CONTENT_TYPE_VALUE]
        return NetworkManager.request(fullUrl, method: .post, headers: headers)
    }
    
}
