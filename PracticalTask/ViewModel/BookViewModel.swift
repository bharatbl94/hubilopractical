//
//  BookViewModel.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 24/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import Foundation
import Alamofire
class BookDataViewModel {

    func getData(pageIndex: Int) ->  APIResponse<BookDataModel>{
        let apiResponse = APIResponse<BookDataModel>()
        RestClient.getBookList(pageIndex: pageIndex).responseObject {
            (response : DataResponse<BookDataModel>) in
            switch response.result{
            case .success(let data):
                if response.response?.statusCode == 200{
                    apiResponse.value = data
                }
            case .failure(let data):
                print(data)
                break
                
            }
        }
        return apiResponse
    }
}
