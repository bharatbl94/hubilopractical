//
//  DetailViewController.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 25/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblCredit: UILabel!
    @IBOutlet weak var imgBookImage: UIImageView!
    @IBOutlet weak var lblBookTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    
    var data: Datum?
    
    class func loadView(_ data: Datum) -> UIViewController{
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        nextViewController.data = data
        return nextViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        lblDepartment.text = data?.departmentTitle
        lblCredit.text = data?.creditLine
        lblBookTitle.text = data?.title
        lblDate.text = data?.dateDisplay
        lblPlace.text = data?.placeOfOrigin
        imgBookImage.loadImage(from: "https://www.artic.edu/iiif/2/\(data?.imageID ?? "")/full/843,/0/default.jpg")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
