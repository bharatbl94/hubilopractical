//
//  ViewController.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 24/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewList: UITableView!
    var dataList : [Datum] = []
    var filterDataList : [Datum] = []
    var responseData : BookDataModel?
    var viewModel : BookDataViewModel!
    var currentPage = 1
    var isDataLoading = false, showFilteredData = false
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblViewList.delegate = self
        tblViewList.dataSource = self
        searchBar.delegate = self
        viewModel = BookDataViewModel()
        let spinner = UIActivityIndicatorView(style:UIActivityIndicatorView.Style.medium)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblViewList.bounds.width, height: CGFloat(44))
        self.tblViewList.tableFooterView = spinner
        self.tblViewList.tableFooterView?.isHidden = true
        
        //Add Refresh Control
        refreshControl.tintColor = UIColor.red
        refreshControl.addTarget(self, action: #selector(refreshUpcomingMatchData(_:)), for: .valueChanged)
        self.tblViewList.addSubview(refreshControl)
        getDataList()
    }
    
    //MARK: REFRESH CONTROL METHOD
    @objc private func refreshUpcomingMatchData(_ sender: Any){
        self.isDataLoading = false
        self.getDataList()
    }
    
    //MARK: API CALLING
    func getDataList(pageIndex: Int = 1) {
        if (isDataLoading) {return}
        if !Utils.isInternetConnected(){
            Utils.showAlertDialog(self, "Alert", networkUnavailable)
            return
        }
        showProgress()
        viewModel.getData(pageIndex: pageIndex).observe{
            response in
            if let data = response {
                self.responseData = data
                self.dataList.append(contentsOf: data.data ?? [])
                self.currentPage = pageIndex
                self.tblViewList.reloadData()
            }
            self.refreshControl.endRefreshing()
            self.hideProgress()
        }
    }
    
    //Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showFilteredData ? filterDataList.count : dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewCell.className, for: indexPath) as! BookTableViewCell
        let dataObject = showFilteredData ? filterDataList[indexPath.row] : dataList[indexPath.row]
        cell.lblBookTitle.text = dataObject.title
        cell.contentView.layer.cornerRadius = 2
        cell.clipsToBounds = true
        cell.bookDescript.text = dataObject.creditLine
        cell.imageBook.loadImage(from: Utils.getImageLink(imageId: dataObject.imageID ?? ""))
        cell.btnSelect.isSelected = dataObject.isSelected
        cell.completionHandler = {
            let dataObject = self.showFilteredData ? self.filterDataList[indexPath.row] :  self.dataList[indexPath.row]
            dataObject.isSelected = !dataObject.isSelected
            if (self.showFilteredData) {
                let index = self.dataList.firstIndex { $0.id == dataObject.id }
                if let index = index {
                    self.dataList[index] = dataObject
                }
            }
            self.tblViewList.reloadRows(at: [indexPath], with: .automatic)
        }
        //        cell.imgCheckIsBookmarked.isHighlighted = dataObject.isSelected
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if indexPath.row == (self.dataList.count ) - 1 {
            if (self.responseData?.pagination?.total ?? 0 > self.dataList.count) && !isDataLoading {
                self.tblViewList.tableFooterView?.isHidden = false
                self.getDataList(pageIndex: self.currentPage + 1)
            }else{
                self.tblViewList.tableFooterView?.isHidden = true
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataObject = showFilteredData ? filterDataList[indexPath.row] :  dataList[indexPath.row]
        let viewController = DetailViewController.loadView(dataObject)
        self.navigationController?.pushViewController(viewController, animated: true)
        //        let dataObject = showFilteredData ? filterDataList[indexPath.row] :  dataList[indexPath.row]
        //        dataObject.isSelected = !dataObject.isSelected
        //        if (showFilteredData) {
        //            let index = dataList.firstIndex { $0.id == dataObject.id }
        //            if let index = index {
        //                dataList[index] = dataObject
        //            }
        //        }
        //        self.tblViewList.reloadRows(at: [indexPath], with: .automatic)
    }
    
    //MARK: Search Bar Changes
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterDataList.removeAll()
        //        self.strSearch = searchText
        self.filterDataList = self.dataList.filter { object in
            return (object.title?.localizedStandardContains(searchText) ?? false)
        }
        self.showFilteredData = searchText.count > 0
        self.tblViewList.reloadData()
    }
    
    @IBAction func rightBarAction(_ sender: Any) {
        let dataList = self.dataList.filter { object in
            return object.isSelected
        }
        let viewController = BookmarkedViewController.loadView(dataList)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func showProgress(){
        isDataLoading = true
        let progressView = CircularProgressView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width:self.view.frame.width, height:  self.view.frame.height)))
        progressView.center = self.view.center
        progressView.animateProgressView()
        progressView.tag = TAG_PROGRESS_VIEW
        self.view.addSubview(progressView)
    }
    
    func hideProgress(){
        isDataLoading = false
        if let progressView = self.view.viewWithTag(TAG_PROGRESS_VIEW){
            progressView.removeFromSuperview()
        }
    }
    
}

