//
//  BookmarkedViewController.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 25/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import UIKit

class BookmarkedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

     @IBOutlet weak var tblViewList: UITableView!
    var dataList : [Datum] = []
      
    class func loadView(_ data: [Datum]) -> UIViewController{
           let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BookmarkedViewController") as! BookmarkedViewController
           nextViewController.dataList = data
           return nextViewController
       }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewList.delegate = self
               tblViewList.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    //Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookTableViewCell.className, for: indexPath) as! BookTableViewCell
        let dataObject = dataList[indexPath.row]
        cell.lblBookTitle.text = dataObject.title
        cell.contentView.layer.cornerRadius = 2
        cell.clipsToBounds = true
        cell.bookDescript.text = dataObject.creditLine
        cell.imageBook.loadImage(from: Utils.getImageLink(imageId: dataObject.imageID ?? ""))
        cell.btnSelect.isSelected = dataObject.isSelected
        //        cell.imgCheckIsBookmarked.isHighlighted = dataObject.isSelected
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            let dataObject = dataList[indexPath.row]
            let viewController = DetailViewController.loadView(dataObject)
            self.navigationController?.pushViewController(viewController, animated: true)
            //        let dataObject = showFilteredData ? filterDataList[indexPath.row] :  dataList[indexPath.row]
    //        dataObject.isSelected = !dataObject.isSelected
    //        if (showFilteredData) {
    //            let index = dataList.firstIndex { $0.id == dataObject.id }
    //            if let index = index {
    //                dataList[index] = dataObject
    //            }
    //        }
    //        self.tblViewList.reloadRows(at: [indexPath], with: .automatic)
        }
        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
