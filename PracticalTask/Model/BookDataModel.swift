//
//  BookDataModel.swift
//  PracticalTask
//
//  Created by Bharat Lalwani on 24/04/21.
//  Copyright © 2021 Bharat. All rights reserved.
//

import Foundation
import ObjectMapper

class BookDataModel : Mappable {
    var data: [Datum]?
    var pagination: Pagination?
    
    init() {
        
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        data <- map["data"]
        pagination <- map["pagination"]
    }
}

class Datum : Mappable {
    
    func mapping(map: Map) {
        id <- map["id"]
        apiModel <- map["apiModel"]
        apiLink <- map["apiLink"]
        title <- map["title"]
        altTitles <- map["altTitles"]
        thumbnail <- map["thumbnail"]
        imageID <- map["image_id"]
        creditLine <- map["credit_line"]
        departmentTitle <- map["department_title"]
        dateDisplay <- map["date_display"]
        placeOfOrigin <- map["place_of_origin"]
    }
    var id: Int?
    var apiModel: String?
    var apiLink: String?
    var isBoosted: Bool?
    var title: String?
    var isSelected = false
    var altTitles: NSNull?
    var thumbnail: Thumbnail?
    var mainReferenceNumber: String?
    var hasNotBeenViewedMuch: Bool?
    var boostRank: NSNull?
    var dateStart, dateEnd: Int?
    var dateDisplay, dateQualifierTitle: String?
    var dateQualifierID: NSNull?
    var artistDisplay, placeOfOrigin, dimensions, mediumDisplay: String?
    var inscriptions: NSNull?
    var creditLine: String?
    var publicationHistory, exhibitionHistory, provenanceText: NSNull?
    var publishingVerificationLevel: String?
    var internalDepartmentID: Int?
    var fiscalYear, fiscalYearDeaccession: NSNull?
    var isPublicDomain, isZoomable: Bool?
    var maxZoomWindowSize: Int?
    var copyrightNotice: NSNull?
    var hasMultimediaResources, hasEducationalResources: Bool?
    var colorfulness: Double?
    var color: Color?
    var latitude, longitude, latlon: NSNull?
    var isOnView: Bool?
    var onLoanDisplay: String?
    var galleryTitle, galleryID, artworkTypeTitle, artworkTypeID: NSNull?
    var departmentTitle, departmentID: String?
    var artistID, artistTitle: NSNull?
    var altArtistIDS, artistIDS, artistTitles: [Any?]?
    var categoryIDS, categoryTitles: [String]?
    var artworkCatalogueIDS: [Any?]?
    var termTitles: [String]?
    var styleID, styleTitle: String?
    var altStyleIDS: [Any?]?
    var styleIDS, styleTitles: [String]?
    var classificationID, classificationTitle: String?
    var altClassificationIDS, classificationIDS, classificationTitles: [String]?
    var subjectID: NSNull?
    var altSubjectIDS, subjectIDS, subjectTitles: [Any?]?
    var materialID: String?
    var altMaterialIDS: [Any?]?
    var materialIDS, materialTitles: [String]?
    var techniqueID: NSNull?
    var altTechniqueIDS, techniqueIDS, techniqueTitles, themeTitles: [Any?]?
    var imageID: String?
    var altImageIDS, documentIDS, soundIDS, videoIDS: [Any?]?
    var textIDS, sectionIDS, sectionTitles, siteIDS: [Any?]?
    var suggestAutocompleteAll: [SuggestAutocompleteAll]?
    var lastUpdatedSource, lastUpdated, timestamp: Date?
    required init?(map: Map) {
        
    }
}

class Pagination : Mappable {
    var total, limit, offset, totalPages: Int?
    var currentPage: Int?
    var nextURL: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        limit <- map["totalPaidAmount"]
        offset <- map["offset"]
        totalPages <- map["totalLoyaltyPointsUsed"]
        currentPage <- map["currentPage"]
        nextURL <- map["nextURL"]
    }
}
class Thumbnail : Mappable {
    var lqip: String?
    var width, height: Int?
    var altText: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lqip <- map["lqip"]
        width <- map["width"]
        height <- map["height"]
        altText <- map["altText"]
    }
}

class Contexts {
    var groupings: [String]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        groupings <- map["groupings"]
    }
}

class SuggestAutocompleteAll {
    var input: [String]?
    var contexts: Contexts?
    var weight: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        input <- map["input"]
        contexts <- map["contexts"]
        weight <- map["weight"]
    }
}

class Color {
    var h, l, s: Int?
    var percentage: Double?
    var population: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        h <- map["h"]
        l <- map["l"]
        s <- map["s"]
        percentage <- map["percentage"]
        population <- map["population"]
    }
}
