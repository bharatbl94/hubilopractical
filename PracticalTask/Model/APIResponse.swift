import Foundation
import ObjectMapper

class APIResponse<T: Mappable>{
    typealias Observer = (T?) -> ()
    var observer: Observer?
    func observe(_ observer: Observer?) {
        self.observer = observer
    }
    
    var value: T? {
        didSet {
            observer?(value)
        }
    }
}
